# CARREFOUR - KATA - MOHAMMED JABRANE

## MVP

### Contraintes technique :

- [x] Spring boot 3.2.2
- [x] Spring Webflux
- [ ] Spring Security
- [x] Liquibase
- [x] Postgresql
- [x] Lombok
- [ ] Docker
- [ ] Kubernetes
- [x] Redis
- [x] Kafka
- [x] Open API
- [x] Hateoas
- [ ] TU - Junit 5 & Mockito
- [ ] TI - Cucumber / Karate
- [ ] TP - Jmeter / Gatling
- [x] Java 21
- [x] Git

## Architecture technique :

### Architecture Hexagonale :  
L'architecture hexagonale, également connue sous le nom de "ports et adaptateurs", est un style architectural de conception logicielle qui favorise la séparation des préoccupations et la modularité du système.

### Open API - First Design:  
L'approche Code First est une approche plus traditionnelle de création d'API, le développement du code ayant lieu une fois les exigences métier définies, générant finalement la documentation à partir du code. L’approche Design First préconise de concevoir le contrat de l’API avant d’écrire un code.

## Use Story :

- [ ] En tant que client, je peux choisir mon mode de livraison.
- [x] Les modes de livraison disponibles sont : `DRIVE`, `DELIVERY`, `DELIVERY_TODAY`, `DELIVERY_ASAP`
- [ ] Les créneaux sont spécifiques au mode de livraison et réservable par d'autres clients.

## À propos du projet
Ce projet présente un exercice intéressant dans le domaine du Tech Lead and Clean Code. Malheureusement, je n'ai pas eu le temps de le compléter comme prévu. Cependant, je compte y revenir dans les prochains jours une fois que j'aurai plus de temps libre pour le finaliser sur tous les axes (Architecture, Use Story, DevOps).

## Captures d'écrans

### Open Api Result :

![img.png](img.png)

### Redis Cache Result :

![img_2.png](img_2.png)

### Kafka Message - Control Center :

![img_1.png](img_1.png)