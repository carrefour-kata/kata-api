package com.carrefour.mohammedjabrane.kataapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;
import org.springframework.kafka.annotation.EnableKafka;

@SpringBootApplication
public class KataApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(KataApiApplication.class, args);
	}

}
