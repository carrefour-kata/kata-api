package com.carrefour.mohammedjabrane.kataapi.adapter.entrypoint.api;

import com.carrefour.mohammedjabrane.kataapi.generated.entrypoint.api.DeliveryModesApi;
import com.carrefour.mohammedjabrane.kataapi.generated.entrypoint.api.dto.DeliveryModeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping
public class DeliveryModeController implements DeliveryModesApi {

    private final FetchDeliveryModeAdapter fetchDeliveryModeAdapter;

    @Autowired
    public DeliveryModeController(FetchDeliveryModeAdapter fetchDeliveryModeAdapter) {
        this.fetchDeliveryModeAdapter = fetchDeliveryModeAdapter;
    }

    @Override
    public Mono<ResponseEntity<Flux<DeliveryModeDTO>>> fetchAllDeliveryMode(final ServerWebExchange exchange) throws ExecutionException, InterruptedException {
        return Mono.just(ResponseEntity.ok().body(fetchDeliveryModeAdapter.fetchAll().toFlux()));
    }

}
