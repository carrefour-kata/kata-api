package com.carrefour.mohammedjabrane.kataapi.adapter.entrypoint.api;

import com.carrefour.mohammedjabrane.kataapi.adapter.entrypoint.api.model.DeliveryModeDTOMapper;
import com.carrefour.mohammedjabrane.kataapi.application.port.entrypoint.api.FetchDeliveryModePort;
import com.carrefour.mohammedjabrane.kataapi.application.port.inbound.ViewerPageInboundPort;
import com.carrefour.mohammedjabrane.kataapi.application.usecase.FetchDeliveryModeUseCase;
import com.carrefour.mohammedjabrane.kataapi.domain.ViewerPage;
import com.carrefour.mohammedjabrane.kataapi.generated.entrypoint.api.dto.DeliveryModeDTO;
import com.carrefour.mohammedjabrane.kataapi.infrastructure.annotation.Adapter;
import com.carrefour.mohammedjabrane.kataapi.infrastructure.reactive.CollectionReactive;

import java.time.LocalDateTime;
import java.util.concurrent.ExecutionException;

/**
 * Read adapter for DeliveryMode
 *
 * @author mohammed jabrane
 * @version 1.0.0
 */
@Adapter
public class FetchDeliveryModeAdapter implements FetchDeliveryModePort {

    private final FetchDeliveryModeUseCase fetchDeliveryModeUseCase;
    private final DeliveryModeDTOMapper deliveryModeDtoMapper;
    private final ViewerPageInboundPort viewerPagePort;

    public FetchDeliveryModeAdapter(FetchDeliveryModeUseCase fetchDeliveryModeUseCase, DeliveryModeDTOMapper deliveryModeDtoMapper, ViewerPageInboundPort viewerPagePort) {
        this.fetchDeliveryModeUseCase = fetchDeliveryModeUseCase;
        this.deliveryModeDtoMapper = deliveryModeDtoMapper;
        this.viewerPagePort = viewerPagePort;
    }

    @Override
    public CollectionReactive<DeliveryModeDTO> fetchAll() throws ExecutionException, InterruptedException {
        viewerPagePort.sendViewerPageEvent(ViewerPage.builder()
                .id("DeliveryMode")
                .dateView(LocalDateTime.now())
                .build());

        return fetchDeliveryModeUseCase.fetchAll()
                .map(deliveryModeDtoMapper::toDto);
    }
}
