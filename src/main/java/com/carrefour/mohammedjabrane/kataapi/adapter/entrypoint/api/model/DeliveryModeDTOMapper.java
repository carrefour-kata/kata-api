package com.carrefour.mohammedjabrane.kataapi.adapter.entrypoint.api.model;

import com.carrefour.mohammedjabrane.kataapi.domain.DeliveryMode;
import com.carrefour.mohammedjabrane.kataapi.generated.entrypoint.api.dto.DeliveryModeDTO;
import com.carrefour.mohammedjabrane.kataapi.infrastructure.annotation.Mapper;

/**
 * Mapper for DeliveryMode entity and domain
 * @author mohammed jabrane
 * @version 1.0.0
 */
@Mapper
public class DeliveryModeDTOMapper {

    DeliveryModeDTOMapper() {
        super();
    }

    public DeliveryModeDTO toDto(DeliveryMode entity) {
        return new DeliveryModeDTO().id(entity.getId().toString()).name(entity.getName());
    }

}
