package com.carrefour.mohammedjabrane.kataapi.adapter.inbound;

import com.carrefour.mohammedjabrane.kataapi.adapter.inbound.model.ViewerPageInboundDTO;
import com.carrefour.mohammedjabrane.kataapi.adapter.inbound.model.ViewerPageInboundDTOMapper;
import com.carrefour.mohammedjabrane.kataapi.application.port.inbound.ViewerPageInboundPort;
import com.carrefour.mohammedjabrane.kataapi.domain.ViewerPage;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutionException;

@Component
@Slf4j
public class ViewerPageInbound implements ViewerPageInboundPort {

    @Value("${topic.name.producer}")
    private String topicName;

    private final KafkaTemplate<String, ViewerPageInboundDTO> kafkaTemplate;
    private final ViewerPageInboundDTOMapper viewerPageDTOMapper;

    public ViewerPageInbound(KafkaTemplate<String, ViewerPageInboundDTO> kafkaTemplate, ViewerPageInboundDTOMapper viewerPageDTOMapper) {
        this.kafkaTemplate = kafkaTemplate;
        this.viewerPageDTOMapper = viewerPageDTOMapper;
    }

    public void sendViewerPageEvent(ViewerPage viewerPage) throws ExecutionException, InterruptedException {
        // Log metadata in context
        // TODO : configuration of logback to take MDC values
        MDC.put("topic.name", topicName);
        MDC.put("topic.message", viewerPage.toString());
        log.info("Send event in topic");

        kafkaTemplate.send(topicName, viewerPageDTOMapper.toDto(viewerPage)).get();
    }
}
