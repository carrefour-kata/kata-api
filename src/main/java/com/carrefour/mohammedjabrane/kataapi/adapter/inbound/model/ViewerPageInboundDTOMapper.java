package com.carrefour.mohammedjabrane.kataapi.adapter.inbound.model;

import com.carrefour.mohammedjabrane.kataapi.domain.ViewerPage;
import com.carrefour.mohammedjabrane.kataapi.infrastructure.annotation.Mapper;

@Mapper
public class ViewerPageInboundDTOMapper {

    ViewerPageInboundDTOMapper() {
        super();
    }

    public ViewerPageInboundDTO toDto(ViewerPage entity) {
        return ViewerPageInboundDTO.builder().id(entity.getId()).dateView(entity.getDateView()).build();
    }
}
