package com.carrefour.mohammedjabrane.kataapi.adapter.outbound;

import com.carrefour.mohammedjabrane.kataapi.adapter.outbound.model.ViewerPageOutboundDTO;
import com.carrefour.mohammedjabrane.kataapi.adapter.outbound.model.ViewerPageOutboundDTOMapper;
import com.carrefour.mohammedjabrane.kataapi.adapter.persistence.viewerpage.SaveViewerPageAdapter;
import com.carrefour.mohammedjabrane.kataapi.application.port.outbound.ViewerPageOutboundPort;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.MDC;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class ViewerPageOutbound implements ViewerPageOutboundPort {

    private final ViewerPageOutboundDTOMapper viewerPageDTOMapper;
    private final SaveViewerPageAdapter saveViewerPageAdapter;

    public ViewerPageOutbound(ViewerPageOutboundDTOMapper viewerPageDTOMapper, SaveViewerPageAdapter saveViewerPageAdapter) {
        this.viewerPageDTOMapper = viewerPageDTOMapper;
        this.saveViewerPageAdapter = saveViewerPageAdapter;
    }

    @KafkaListener(topics = "${topic.name.consumer}", groupId = "${spring.kafka.consumer.group-id}")
    public void consumeViewerPageEvent(ConsumerRecord<String, String> payload) throws JsonProcessingException {
        MDC.put("topic.outbound.key", payload.key());
        MDC.put("topic.outbound.headers", payload.headers().toString());
        MDC.put("topic.outbound.partition", Flux.just(payload.partition()).toString());
        log.info("Consume event in topic");

        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.registerModule(new JavaTimeModule());
        final var viewerPage = viewerPageDTOMapper.toEntity(objectMapper.readValue(objectMapper.writeValueAsString(payload.value()), ViewerPageOutboundDTO.class));
        saveViewerPageAdapter.addViewerPage(viewerPage);
    }
}
