package com.carrefour.mohammedjabrane.kataapi.adapter.outbound.model;

import com.carrefour.mohammedjabrane.kataapi.domain.ViewerPage;
import com.carrefour.mohammedjabrane.kataapi.infrastructure.annotation.Mapper;

@Mapper
public class ViewerPageOutboundDTOMapper {

    ViewerPageOutboundDTOMapper() {
        super();
    }

    public ViewerPage toEntity(ViewerPageOutboundDTO dto) {
        return ViewerPage.builder().id(dto.getId()).dateView(dto.getDateView()).build();
    }
}
