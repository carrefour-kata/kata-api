package com.carrefour.mohammedjabrane.kataapi.adapter.persistence.deliverymode;

import com.carrefour.mohammedjabrane.kataapi.adapter.persistence.deliverymode.model.DeliveryModes;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Repository for DeliveryMode
 * @author mohammed jabrane
 * @version 1.0.0
 */
@Repository(value = "deliveryModeRepository")
public interface DeliveryModeRepository extends R2dbcRepository<DeliveryModes, UUID> {
}
