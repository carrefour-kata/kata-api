package com.carrefour.mohammedjabrane.kataapi.adapter.persistence.deliverymode;

import com.carrefour.mohammedjabrane.kataapi.adapter.persistence.deliverymode.model.DeliveryModeEntityMapper;
import com.carrefour.mohammedjabrane.kataapi.adapter.persistence.deliverymode.model.DeliveryModes;
import com.carrefour.mohammedjabrane.kataapi.application.port.persistence.deliverymode.ReadDeliveryModePort;
import com.carrefour.mohammedjabrane.kataapi.domain.DeliveryMode;
import com.carrefour.mohammedjabrane.kataapi.infrastructure.annotation.Adapter;
import com.carrefour.mohammedjabrane.kataapi.infrastructure.reactive.CollectionReactive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.ReactiveRedisTemplate;

/**
 * Read adapter for DeliveryMode
 * @author mohammed jabrane
 * @version 1.0.0
 */
@Adapter
public class ReadDeliveryModeAdapter implements ReadDeliveryModePort {

    private final DeliveryModeRepository deliveryModeRepository;

    private final DeliveryModeEntityMapper deliveryModeEntityMapper;

    private final ReactiveRedisTemplate<String, DeliveryModes> reactiveRedisTemplate;

    @Autowired
    public ReadDeliveryModeAdapter(@Qualifier("deliveryModeRepository") DeliveryModeRepository deliveryModeRepository, DeliveryModeEntityMapper deliveryModeEntityMapper, ReactiveRedisTemplate<String, DeliveryModes> reactiveRedisTemplate) {
        this.deliveryModeRepository = deliveryModeRepository;
        this.deliveryModeEntityMapper = deliveryModeEntityMapper;
        this.reactiveRedisTemplate = reactiveRedisTemplate;
    }

    @Override
    public CollectionReactive<DeliveryMode> fetchAll() {
        return CollectionReactive.of(reactiveRedisTemplate.keys("delivery_modes:*")
                .flatMap(key -> reactiveRedisTemplate.opsForValue().get(key))
                .switchIfEmpty(deliveryModeRepository.findAll()
                        .flatMap(deliveryMode ->
                                reactiveRedisTemplate
                                        .opsForValue()
                                        .set("delivery_modes:" + deliveryMode.getId(), deliveryMode)
                        )
                        .thenMany(reactiveRedisTemplate
                                .keys("delivery_modes:*")
                                .flatMap(key -> reactiveRedisTemplate.opsForValue().get(key))
                        )
                ))
                .map(deliveryModeEntityMapper::toDomain);
    }
}
