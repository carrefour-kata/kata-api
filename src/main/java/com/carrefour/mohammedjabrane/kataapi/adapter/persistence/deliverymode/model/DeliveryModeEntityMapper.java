package com.carrefour.mohammedjabrane.kataapi.adapter.persistence.deliverymode.model;

import com.carrefour.mohammedjabrane.kataapi.adapter.persistence.deliverymode.model.DeliveryModes;
import com.carrefour.mohammedjabrane.kataapi.domain.DeliveryMode;
import com.carrefour.mohammedjabrane.kataapi.infrastructure.annotation.Mapper;

/**
 * Mapper for DeliveryMode entity and domain
 * @author mohammed jabrane
 * @version 1.0.0
 */
@Mapper
public class DeliveryModeEntityMapper {

    DeliveryModeEntityMapper() {
        super();
    }

    public DeliveryMode toDomain(DeliveryModes entity) {
        return DeliveryMode.builder()
                .id(entity.getId())
                .name(entity.getName())
                .build();
    }

}
