package com.carrefour.mohammedjabrane.kataapi.adapter.persistence.deliverymode.model;

import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

/**
 * DeliveryMode's entity
 * @author mohammed jabrane
 * @version 1.0.0
 */
@Data
@Table
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DeliveryModes {

    @Id
    private UUID id;

    private String name;

}
