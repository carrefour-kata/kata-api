package com.carrefour.mohammedjabrane.kataapi.adapter.persistence.viewerpage;

import com.carrefour.mohammedjabrane.kataapi.adapter.persistence.viewerpage.model.ViewerPages;
import com.carrefour.mohammedjabrane.kataapi.application.port.persistence.viewerpager.SaveViewerPagerPort;
import com.carrefour.mohammedjabrane.kataapi.domain.ViewerPage;
import com.carrefour.mohammedjabrane.kataapi.infrastructure.annotation.Adapter;
import reactor.core.publisher.Mono;

import java.util.UUID;

/**
 * Save adapter for Viewer Pages
 *
 * @author mohammed jabrane
 * @version 1.0.0
 */
@Adapter
public class SaveViewerPageAdapter implements SaveViewerPagerPort {

    private final ViewerPageRepository viewerPageRepository;

    public SaveViewerPageAdapter(ViewerPageRepository viewerPageRepository) {
        this.viewerPageRepository = viewerPageRepository;
    }

    @Override
    public void addViewerPage(final ViewerPage viewerPage) {
        viewerPageRepository.findByPage(viewerPage.getId())
                .flatMap(item -> {
                    item.setViewer(item.getViewer() + 1);
                    return viewerPageRepository.save(item);
                })
                .switchIfEmpty(Mono.defer(() -> viewerPageRepository.save(ViewerPages.builder()
                        .id(UUID.randomUUID())
                        .page(viewerPage.getId())
                        .viewer(1)
                        .build())));
    }
}
