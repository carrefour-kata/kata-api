package com.carrefour.mohammedjabrane.kataapi.adapter.persistence.viewerpage;

import com.carrefour.mohammedjabrane.kataapi.adapter.persistence.viewerpage.model.ViewerPages;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Mono;

import java.util.UUID;

/**
 * Repository for ViewerPage
 *
 * @author mohammed jabrane
 * @version 1.0.0
 */
public interface ViewerPageRepository extends R2dbcRepository<ViewerPages, UUID> {

    Mono<ViewerPages> findByPage(String name);
}
