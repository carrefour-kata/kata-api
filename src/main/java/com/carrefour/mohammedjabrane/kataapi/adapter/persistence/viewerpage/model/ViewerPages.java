package com.carrefour.mohammedjabrane.kataapi.adapter.persistence.viewerpage.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

/**
 * ViewerPage's entity
 *
 * @author mohammed jabrane
 * @version 1.0.0
 */
@Data
@Entity
@Table(name = "viewer_pages")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ViewerPages {

    @Id
    private UUID id;

    private String page;

    private Integer viewer;

}
