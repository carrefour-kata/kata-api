package com.carrefour.mohammedjabrane.kataapi.application.port.entrypoint.api;

import com.carrefour.mohammedjabrane.kataapi.generated.entrypoint.api.dto.DeliveryModeDTO;
import com.carrefour.mohammedjabrane.kataapi.infrastructure.reactive.CollectionReactive;

import java.util.concurrent.ExecutionException;

public interface FetchDeliveryModePort {

    CollectionReactive<DeliveryModeDTO> fetchAll() throws ExecutionException, InterruptedException;

}
