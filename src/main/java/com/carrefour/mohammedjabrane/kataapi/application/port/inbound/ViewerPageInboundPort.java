package com.carrefour.mohammedjabrane.kataapi.application.port.inbound;

import com.carrefour.mohammedjabrane.kataapi.domain.ViewerPage;

import java.util.concurrent.ExecutionException;

public interface ViewerPageInboundPort {

    void sendViewerPageEvent(ViewerPage viewerPage) throws ExecutionException, InterruptedException;
}
