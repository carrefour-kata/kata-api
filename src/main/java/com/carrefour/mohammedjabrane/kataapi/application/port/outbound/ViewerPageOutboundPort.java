package com.carrefour.mohammedjabrane.kataapi.application.port.outbound;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.kafka.clients.consumer.ConsumerRecord;

public interface ViewerPageOutboundPort {

    void consumeViewerPageEvent(ConsumerRecord<String, String> payload) throws JsonProcessingException;
}
