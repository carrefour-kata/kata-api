package com.carrefour.mohammedjabrane.kataapi.application.port.persistence.deliverymode;

import com.carrefour.mohammedjabrane.kataapi.domain.DeliveryMode;
import com.carrefour.mohammedjabrane.kataapi.infrastructure.reactive.CollectionReactive;

public interface ReadDeliveryModePort {

    CollectionReactive<DeliveryMode> fetchAll();

}
