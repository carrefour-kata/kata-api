package com.carrefour.mohammedjabrane.kataapi.application.port.persistence.viewerpager;

import com.carrefour.mohammedjabrane.kataapi.domain.ViewerPage;

public interface SaveViewerPagerPort {

    void addViewerPage(ViewerPage viewerPage);
}
