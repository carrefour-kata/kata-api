package com.carrefour.mohammedjabrane.kataapi.application.service;

import com.carrefour.mohammedjabrane.kataapi.application.port.persistence.viewerpager.SaveViewerPagerPort;
import com.carrefour.mohammedjabrane.kataapi.application.usecase.AddUserViewerPageUseCase;
import com.carrefour.mohammedjabrane.kataapi.domain.ViewerPage;
import org.springframework.stereotype.Service;

@Service
public class AddViewerPageService implements AddUserViewerPageUseCase {

    private final SaveViewerPagerPort saveViewerPagerPort;

    public AddViewerPageService(SaveViewerPagerPort saveViewerPagerPort) {
        this.saveViewerPagerPort = saveViewerPagerPort;
    }

    @Override
    public void addViewerPage(ViewerPage viewerPage) {
        saveViewerPagerPort.addViewerPage(viewerPage);
    }
}
