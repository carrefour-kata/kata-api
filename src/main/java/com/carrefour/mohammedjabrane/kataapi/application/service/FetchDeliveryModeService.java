package com.carrefour.mohammedjabrane.kataapi.application.service;

import com.carrefour.mohammedjabrane.kataapi.application.port.persistence.deliverymode.ReadDeliveryModePort;
import com.carrefour.mohammedjabrane.kataapi.application.usecase.FetchDeliveryModeUseCase;
import com.carrefour.mohammedjabrane.kataapi.domain.DeliveryMode;
import com.carrefour.mohammedjabrane.kataapi.infrastructure.reactive.CollectionReactive;
import org.springframework.stereotype.Service;

@Service
public class FetchDeliveryModeService implements FetchDeliveryModeUseCase {

    private final ReadDeliveryModePort readDeliveryModePort;

    public FetchDeliveryModeService(ReadDeliveryModePort readDeliveryModePort) {
        this.readDeliveryModePort = readDeliveryModePort;
    }

    @Override
    public CollectionReactive<DeliveryMode> fetchAll() {
        return readDeliveryModePort.fetchAll();
    }

}
