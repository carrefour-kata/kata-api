package com.carrefour.mohammedjabrane.kataapi.application.usecase;

import com.carrefour.mohammedjabrane.kataapi.domain.ViewerPage;

public interface AddUserViewerPageUseCase {

    void addViewerPage(ViewerPage viewerPage);
}
