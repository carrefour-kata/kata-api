package com.carrefour.mohammedjabrane.kataapi.application.usecase;

import com.carrefour.mohammedjabrane.kataapi.domain.DeliveryMode;
import com.carrefour.mohammedjabrane.kataapi.infrastructure.reactive.CollectionReactive;

public interface FetchDeliveryModeUseCase {

    CollectionReactive<DeliveryMode> fetchAll();

}
