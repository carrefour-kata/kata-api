package com.carrefour.mohammedjabrane.kataapi.application.usecase;

import com.carrefour.mohammedjabrane.kataapi.domain.ViewerPage;
import com.carrefour.mohammedjabrane.kataapi.infrastructure.reactive.CollectionReactive;

public interface FetchUsersViewerPageUseCase {

    CollectionReactive<ViewerPage> fetchAll();
}
