package com.carrefour.mohammedjabrane.kataapi.domain;

import lombok.*;

import java.io.Serial;
import java.io.Serializable;
import java.util.UUID;

/**
 * DeliveryMode's domain
 * @author mohammed jabrane
 * @version 1.0.0
 */
@Data
@AllArgsConstructor
@Builder
public class DeliveryMode implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private final UUID id;

    private final String name;

}
