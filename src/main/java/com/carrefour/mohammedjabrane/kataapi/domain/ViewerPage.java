package com.carrefour.mohammedjabrane.kataapi.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@Builder
public class ViewerPage {

    private String id;

    private LocalDateTime dateView;
}
