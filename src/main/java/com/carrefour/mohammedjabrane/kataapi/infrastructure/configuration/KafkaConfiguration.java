package com.carrefour.mohammedjabrane.kataapi.infrastructure.configuration;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
@EnableKafka
public class KafkaConfiguration {

    @Bean
    public NewTopic topic(){
        return TopicBuilder.name("kata-topic")
                .partitions(1)
                .replicas(2)
                .build();
    }
}
