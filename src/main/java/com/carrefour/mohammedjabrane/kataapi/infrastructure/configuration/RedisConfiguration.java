package com.carrefour.mohammedjabrane.kataapi.infrastructure.configuration;

import com.carrefour.mohammedjabrane.kataapi.adapter.persistence.deliverymode.model.DeliveryModes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;

@Configuration
public class RedisConfiguration {

    @Bean
    public ReactiveRedisTemplate<String, DeliveryModes> reactiveRedisTemplate(ReactiveRedisConnectionFactory factory) {
        Jackson2JsonRedisSerializer<DeliveryModes> serializer = new Jackson2JsonRedisSerializer<>(DeliveryModes.class);
        RedisSerializationContext.RedisSerializationContextBuilder<String, DeliveryModes> builder =RedisSerializationContext.newSerializationContext(new Jackson2JsonRedisSerializer<>(String.class));
        RedisSerializationContext<String, DeliveryModes> context = builder.value(serializer).build();
        return new ReactiveRedisTemplate<>(factory, context);
    }

}