package com.carrefour.mohammedjabrane.kataapi.infrastructure.reactive;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Value;
import reactor.core.publisher.Flux;

import java.util.function.Function;

/**
 * Class for manage generic operations on reactive collections data
 * @param <T>
 * @author "Mohammed JABRANE"
 * @version 1.0.0
 */
@Getter(AccessLevel.PRIVATE)
@Value(staticConstructor = "of")
public class CollectionReactive<T> {

    Flux<T> flux;

    public Flux<T> toFlux() {
        return flux;
    }

    public <U> CollectionReactive<U> map(Function<? super T, ? extends U> mapper) {
        return CollectionReactive.of(flux.map(mapper));
    }

}
