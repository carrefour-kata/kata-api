package com.carrefour.mohammedjabrane.kataapi.infrastructure.utility;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.LocalDateTime;

public class LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {

    @Override
    public LocalDateTime deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        int[] dateTimeArray = jp.readValueAs(int[].class);
        if (dateTimeArray.length != 7) {
            throw new IOException("Invalid date-time array length: " + dateTimeArray.length);
        }
        return LocalDateTime.of(dateTimeArray[0], dateTimeArray[1], dateTimeArray[2], dateTimeArray[3], dateTimeArray[4], dateTimeArray[5], dateTimeArray[6]);
    }

}
